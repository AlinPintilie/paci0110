import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import org.junit.*;

public class CartiRepoTest {

    private CartiRepo repo;
    private Carte carte;
    private Carte carte2;

    @BeforeClass
    public static void setupAll() {
        System.out.println("Initializarea tuturor testelor");

    }

    @AfterClass
    public static void closeAll() {
        System.out.println("Inchiderea tuturor testelor");

    }


    @Before
    public void setUp() throws Exception {
        carte = new Carte();
      //  carte.setTitlu("Parabole");
        carte.adaugaAutor("Ion Agarbiceanu");
        carte.setAnAparitie("18895");
        carte.setEditura("parabole");
        carte.adaugaCuvantCheie("poezie, eminescu");


        carte2 = new Carte();
        carte2.adaugaAutor("");
        carte2.setAnAparitie("");
        carte2.setEditura("poezii");
        carte2.adaugaCuvantCheie("");
        repo = new CartiRepo();
        System.out.println("Initializare cu succes");
    }

    @After
    public void tearDown() throws Exception {
        carte = null;
        System.out.println("S-a facut anularea");
    }


    @Test
    public void adaugaCarte() {
        System.out.println("au fost "+ repo.getCarti().size()+" carti");
        repo.adaugaCarte(carte);
        System.out.println("acum sunt " +repo.getCarti().size()+" carti");
        System.out.println("s-a facut adaugarea");
    }

    @Test (timeout=10)//passed
    public void testDivision() {
        System.out.println("Se testeaza viteza de executie");
        try {
            repo.adaugaCarte(carte);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(timeout=100)//failed
    public void infinity() {
        while(true);
    }




    @Test
    public void adaugaCarte2() {
        System.out.println("testare cu date invalide");
        System.out.println("au fost "+ repo.getCarti().size()+" carti");
        repo.adaugaCarte(carte2);
        System.out.println("acum sunt " +repo.getCarti().size()+" carti");
        System.out.println("s-a facut adaugarea");
    }



}