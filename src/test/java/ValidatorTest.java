import BibliotecaApp.util.Validator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class ValidatorTest {

    private Validator validator ;


    @BeforeClass
    public static void setupAll(){
        System.out.println("Initializarea tuturor testelor");

    }

    @AfterClass
    public static void closeAll(){
        System.out.println("Inchiderea tuturor testelor");

    }


    @org.junit.Before
    public void setUp() throws Exception {
        validator = new Validator();
    }

    @org.junit.After
    public void tearDown() throws Exception {
        validator = null;
    }

    @org.junit.Test
    public void isStringOK() {

        try {
            assertTrue(validator.isStringOK("abc"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("isString passed ok");
    }

    @Test
    public void isNumber() {
        assertTrue(validator.isNumber("34"));
        System.out.println("isNumber passed ok");

    }

}
