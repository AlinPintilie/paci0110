package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class CartiRepoMockTest {

  Carte c1, c2;
  CartiRepoMock cartiRepoMock;

  @BeforeClass
  public static void setUpAll() {
    System.out.println("Setup before all subsequent tests !");
  }

  @Before
  public void setUp() throws Exception {
    c1 = new Carte();
    c1.setTitlu("Povesti");
    c1.adaugaAutor("i Creanga");
    c1.setAnAparitie("1877");
    c1.setEditura("Corint");
    c1.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poem", "poezie")));

    c2 = new Carte();
    c2.setTitlu("Plummb");
    c2.adaugaAutor("Bacovia");
    c2.setAnAparitie("2020");
    c2.setEditura("Humanitas");
    c2.setCuvinteCheie(new ArrayList<String>(Arrays.asList("Bacovia", "simbolism")));

    cartiRepoMock = new CartiRepoMock();
  }

  @After
  public void tearDown() throws Exception {
    c1 = null;
    c2 = null;
    cartiRepoMock = null;

  }

  @AfterClass
  public static void tearDownAll() throws Exception {
    System.out.println("Tearing all down");
  }

  @Test
  public void cautaCarteCandRepoEsteGol() {
    assertEquals("Nu exista carti in repository", 0, cartiRepoMock.cautaCarte("Eminescu").size());
    System.out.println("Repository gol succesfully checked.");
  }

  @Test
  public void cautaCarteDupaAutorSiGaseste() {
    cartiRepoMock.adaugaCarte(c1);  //metoda apelata doar pentru a adauga
    cartiRepoMock.adaugaCarte(c2);
    assertEquals("Autorul a fost gasit in Repository.", 1, cartiRepoMock.cautaCarte("I Creanga").size());
    System.out.println("Autorul a fost gasit in lista repository");
  }

  @Test
  public void cautaCarteDupaAutorInexistent() {
    cartiRepoMock.adaugaCarte(c1);
    assertEquals("Autorul nu a fost gasit.", 0, cartiRepoMock.cautaCarte("Eliade").size());
    System.out.println("Autorul nu exista in lista repository.");
  }

}
